'use strict'

angular.module('clms.user-mgt',['ngMessages', 'md.data.table'])

    .controller('mainCtrl', function ($mdDialog,$window ) {

        const self = this;



        if(sessionStorage.getItem('permission') == 'admin'){
            self.isAdmin = true;
            self.isTeacher = false;
            self.isparent = false;
        }
        else if(sessionStorage.getItem('permission') == 'teacher'){
            self.isAdmin = false;
            self.isTeacher = true;
            self.isparent = false;

        } else if(sessionStorage.getItem('permission') == 'parent'){
            self.isAdmin = false;
            self.isTeacher = false;
            self.isparent = true;
        }


        self.showPostNoticeDialog = function (event) {

            $mdDialog.show({
                controller: "noticeCtrl as notice",
                templateUrl: "./app/user-mgt/templates/post-notice.html",
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
            })
                .then(function () {

                }, function () {

                });
        }
    })

    .controller('loginCtrl', function (Auth, $timeout, $location, $rootScope, $mdToast, $window) {

        const self = this;

        self.errorMessage = null;
        self.formData = {
            username: "",
            password: ""
        };

        self.response = {};

        self.doLogin = function (loginData) {
            if (loginData) {

                if(loginData.username == 'ishan@shilpa.com'){

                    if(loginData.password == 'pass123'){

                        self.response.username = loginData.username;
                        self.response.name = 'Ishan Yapa';
                        self.response.permission = 'admin';
                        self.response.success = true;
                        self.response.message = 'User authenticated';
                    }
                }

                else if(loginData.username == 'danika@shilpa.com'){

                    if(loginData.password == 'pass123'){
                        self.response.username = loginData.username;
                        self.response.name = 'Danika Rathnayka';
                        self.response.permission = 'teacher';
                        self.response.success = true;
                        self.response.message = 'User authenticated';
                    }
                }

                else if(loginData.username == 'isuru@shilpa.com'){

                    if(loginData.password == 'pass123'){
                        self.response.username = loginData.username;
                        self.response.name = 'Isuru Prasad';
                        self.response.permission = 'parent';
                        self.response.success = true;
                        self.response.message = 'User authenticated';
                    }
                }

                else{
                    self.response.success = false;
                    self.response.message = 'Can not authenticate';
                }

                if (self.response.success === true) {

                    self.showLoginToast(self.response.message, 'success-toast')

                    $timeout (function () {

                            $window.sessionStorage.setItem('username',self.response.username);
                            $window.sessionStorage.setItem('name',self.response.name);
                            $window.sessionStorage.setItem('permission',self.response.permission);

                            $location.path('/dashboard/administrator/console');

                    }, 2000);
                } else if (self.response.success === false) {

                    self.showLoginToast(self.response.message, 'error-toast');

                }
            }
        }
        
        self.showLoginToast = function (message, theme) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position('bottom')
                    .theme(theme)
                    .hideDelay(1500)
                    .parent('toastParent')
            );
        }

    })

    .controller('userCtrl', function (User, $mdToast, $scope, $mdDialog, $timeout) {

        const self = this;

        self.selectedIndex = 0;

        self.enableUpdateTab = false;

        self.userModel = {

            username: "",
            password: "",
            confirmPassword: "",
            name: "",
            email: "",
            permission: "",
            studentId: "",
            class: "",
            level: ""

        }

        self.updateUserModel = {

            name: "",
            email: "",
            permission: "",
            studentId: "",
            class: "",
            level: ""

        }

        self.isFieldRequired = false;
        self.isUpdateFieldRequired = false;

        // Md-table config
        self.users = [
            {'username':'isuru','name':'Isuru Prasad','email':'isuru@gmail.com','permission':'teacher'}
            ];

        self.selected = [];

        self.limitOptions = [5, 10, 15];

        self.query = {
            order: 'username',
            limit: 5,
            page: 1
        };
        // End of Md-table config


        // Add a new user to the system
        self.addNewUser = function (userData) {

            $scope.userForm.$setPristine();
            $scope.userForm.$setUntouched();

            self.showToast('success-toast', 'User added');
            
        }



        // Delete selected users
        self.deleteSelectedUsers = function () {
            self.users = [];
            self.showToast('success-toast', 'User deleted');

        };

        // Confirmation dialog for deleting users
        self.showDeleteConfirm = function (event) {
            
            var confirm = $mdDialog.confirm()
                .title('Do you need to delete the selected users ?')
                .textContent('All the selected users will be deleted if you choose yes.')
                .targetEvent(event)
                .ok('YES')
                .cancel('NO');
            
            $mdDialog.show(confirm).then(function () {

                self.deleteSelectedUsers();
                $scope.userTable.$setPristine();
                $scope.userTable.$setUntouched();
                
            }, function () {

            })
            
        }

        self.showCreateGroupDialog = function (event) {

            $mdDialog.show({
                controller: "groupCtrl as group",
                templateUrl: "./app/user-mgt/templates/add-group.html",
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
            })
                .then(function () {

                }, function () {

                });


        };

        self.navigateToTab = function (index) {

            self.selectedIndex = index;
            Object.assign(self.updateUserModel, self.selected[0]);

            self.isSelected();

        }

        self.isSelected = function () {

            if ( self.selected.length > 0 ) {

                self.enableUpdateTab = true;

            } else {

                self.showUpdateTab = null;

            }
        }

        self.updateSelectedUser = function () {

            self.showToast('success-toast', 'Updated');

        };

        $scope.$watch('selected', function () {
            User.setSelectedUsers(self.selected);
        });


        self.showToast = function(type, message){

            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position('bottom')
                    .theme(type)
                    .hideDelay(1500)
                    .parent('userForm')

            );

        }

    })

    .controller('groupCtrl', function (User, Group, $mdDialog, $timeout, $mdToast) {

        const self = this;

        self.readonly = false;
        self.selectedItem = null;
        self.searchText = null;
        self.querySearch = querySearch;
        self.groups = loadGroups();
        self.selectedGroups = [];
        self.numberChips = [];
        self.numberChips2 = [];
        self.numberBuffer = '';
        self.autocompleteDemoRequireMatch = true;
        self.transformChip = transformChip;

        self.initiatedGroup = {

            groupName: "",
            description: "",
            allowedSections: self.selectedGroups,
            members: User.getSelectedUsers()

        }

        /**
         * Return the proper object when the append is called.
         */
        function transformChip(chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return { name: chip, type: 'new' }
        }

        /**
         * Search for groups.
         */
        function querySearch (query) {
            var results = query ? self.groups.filter(createFilterFor(query)) : [];
            return results;
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(groups) {
                return (groups._lowername.indexOf(lowercaseQuery) === 0)
            };

        }

        // Cancels the dialog
        self.cancel = function () {
            $mdDialog.cancel();
        }

        function loadGroups() {
            var groups = [
                {
                    'name': 'User Management',
                },
                {
                    'name': 'Classroom Management',
                },
                {
                    'name': 'Grade Management',
                },
                {
                    'name': 'Notifications',
                },
                {
                    'name': 'Personal Storage'
                },
                {
                    'name': 'Analytics'
                }
            ];

            return groups.map(function (grp) {
                grp.sectionName = grp.name;
                grp._lowername = grp.name.toLowerCase();
                return grp;
            });
        }

        self.createUserGroup = function () {

            Group.createGroup(self.initiatedGroup).then(function (response) {

                if (response.status == 406) {

                    self.showToast('error-toast', response.data.message, 'addGroup');

                } else {

                    $timeout(function () {

                        self.cancel();
                        self.showToast('success-toast', response.data.message, 'viewUsers');

                    },1000)
                }

            }). catch(function (err) {

                self.showToast('error-toast', response.data.message, 'addGroup');

            })
        }

        self.showToast = function(type, message, parent){

            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position('bottom')
                    .theme(type)
                    .hideDelay(1500)
                    .parent(parent)

            );

        }
    })

    .controller('serviceCtrl', function (Mail, $mdDialog, $rootScope, $scope) {

        const self = this;
        self.mails = [
            {'type':'unread','subject':'regarding permission issue','from':'Admin','to':'You','message':'test message'},
            {'type':'read','subject':'regarding password changing','from':'Admin','to':'You','message':'test message'}
        ];

        $rootScope.$on("GetAllMails", function () {

            self.getMails();

        });

        self.getMails = function () {

            Mail.getAllMails().then(function (response) {

                self.mails = response.data;
                console.log(self.mails);

                self.mails.sort(function(a){
                    return new Date() - new Date(a.date);
                });

            });

        };
        //self.getMails();

        self.showViewMailDialog = function (event, index) {

            Mail.setMail(self.mails[index]);

            $mdDialog.show({
                controller: "viewMailCtrl as viewMail",
                templateUrl: "./app/user-mgt/templates/view-mail.html",
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
            })
                .then(function () {

                }, function () {

                });


        };

        self.showSendMailDialog = function (event) {

            $mdDialog.show({
                controller: "sendMailCtrl as send",
                templateUrl: "./app/user-mgt/templates/send-mail.html",
                parent: angular.element(document.body),
                targetEvent: event,
                clickOutsideToClose: true,
            })
                .then(function () {

                }, function () {

                });
        }

    })

    .controller('viewMailCtrl', function ($mdDialog, Mail, $rootScope) {

        const self = this;

        self.newMail = Mail.getMail();

        self.cancel = function () {

           // $rootScope.$broadcast("GetAllMails", {});

            $mdDialog.cancel();

        }
    })

    .controller('sendMailCtrl', function ($mdDialog, Mail, $mdToast, $timeout) {

        const self = this;

        self.mail = {

            toAddress: "",
            subject: "",
            message: ""

        };

        self.sendMail = function () {
            self.showToast('success-toast', 'Email sent', 'viewMail');
            self.cancel();

        self.cancel = function () {

            $mdDialog.cancel();

        }
        }

        self.showToast = function(type, message, parent){

            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position('bottom')
                    .theme(type)
                    .hideDelay(1500)
                    .parent(parent)

            );

        }

    })

    .controller('noticeCtrl', function ($mdDialog, User, $mdToast, $timeout) {



    })

    .controller('logCtrl', function (Auth) {
    })