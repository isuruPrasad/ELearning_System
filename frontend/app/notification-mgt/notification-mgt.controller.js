'use strict'

angular.module('clms.notification-mgt')

    .controller('notificationCtrl',function (Notification,$location,$mdToast, $window) {

        const self = this;

        let user_role = $window.sessionStorage.getItem('permission');

        if(user_role == 'parent'){
            self.isParent = true;
            self.isTeacher = false;
        }else if(user_role == 'teacher'){
            self.isParent = false;
            self.isTeacher = true;
        }

        //get current user name
        self.logedUser = sessionStorage.getItem('username');

        self.notifications=[{
            'title':'Term test', 'description':'term test will be held on next week Monday.', 'publishedDateTime':'2017-10-12', 'by':'Mis.Latha'
        },
            {
                'title':'Sport meet', 'description':'Sport meet will be held on next week Monday.', 'publishedDateTime':'2017-11-12', 'by':'Miss.Thoradeniya'
            }];

        self.notificationForm = {};

        self.formData ={
            username : "",
            class : "",
            publishedDateTime : "",
            description : "",
            title :""
        };

        //store all available classes
        self.classes = [
            {"className":"A"},
            {"className":"B"},
            {"className":"C"},
        ];
        self.userClass="";

        //reset fields
        self.resetAddNotification = function () {

            //reset input fields
            self.formData.username = "";
                self.formData.title = "";
            self.formData.class = "";
            self.formData.description = "";
        };

        //validate and save notifications
        self.save = function (details) {

            //set username
                self.formData.username = self.logedUser;
            //set date
            self.formData.publishedDateTime = new Date();

            //validate
            if(self.formData.username != "" &&  self.formData.username != undefined &&
                self.formData.class != "" && self.formData.class != undefined &&
                self.formData.publishedDateTime != "" && self.formData.publishedDateTime != undefined &&
                self.formData.description != "" && self.formData.description != undefined &&
                self.formData.title != "" && self.formData.title != undefined
            ){

                //show message
                self.showToastMessage('success-toast', 'Notification published');

                self.resetAddNotification();

                self.notificationForm.$setPristine();
                self.notificationForm.$setUntouched();
            }

        }

        //show Toast message
        self.showToastMessage = function(type, message){

            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position('bottom')
                    .theme(type)
                    .hideDelay(1500)
            );

        }
    })