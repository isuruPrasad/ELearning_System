'use strict';

angular.module('clms.reminder-mgt',[])
    .controller('reminderCtrl',function(Reminder,$mdDialog,$mdToast,$scope,$location) {

        const self = this;

        self.reminders = [{
            'title':'Preschool fees','description':'pay fees before tomorrow','remindDate':'2017.12.02'
        }];
        self.loggedUser = sessionStorage.getItem('username');
        self.reminderStatus = false;
        self.showAll = false;
        self.selectedItem = {};


        self.formData = {
            username : "",
            title : "",
            remindDate : "",
            description : ""
        };

        self.isShowAll = false;
        //set a min date for calander
        self.minDate = new Date();

        //set logged user name
        self.formData.username = self.loggedUser;

        //reset fields
        self.resetAddReminderForm = function () {
            //reset input fields
            self.formData.username = "";
            self.formData.title = "";
            self.formData.description = "";
            self.formData.remindDate = "";
        };

        self.fillTable = function () {
;
            if(self.isShowAll){

                self.reminders = [{
                    'title':'Preschool fees','description':'pay fees before tomorrow','remindDate':'2017.12.02'
                }];
                self.isShowAll = false;

            }else{

                self.reminders = [
                    {'title':'Preschool fees','description':'pay fees before tomorrow','remindDate':'2017.12.02'},
                    {'title':'Home work','description':'check home works','remindDate':'2017.10.02'}
                ];
                self.isShowAll = true;
            }
        }

        //save reminder
        self.addNewReminder = function (reminderDeatils){

            if (self.formData.title != "" &&  self.formData.title != undefined &&
                self.formData.remindDate != "" && self.formData.title != undefined &&
                self.formData.descriptions != "" &&  self.formData.title != undefined &&
                self.formData.username != ""  && self.formData.title != undefined ) {

                $scope.addReminderForm.$setPristine();
                $scope.addReminderForm.$setUntouched();

                self.showToastMessage('Reminder added','success-toast');
            }

        };

        //show selected reminder on edit window
        self.showOnEditMode = function(item,ev) {

            self.selectedItem = item;

            $mdDialog
                .show({

                controller: popupController,
                templateUrl: 'app/reminder-mgt/templates/edit-reminder.html',
                parent: angular.element(document.body),
                    targetEvent: ev,
                clickOutsideToClose:true,
                    fullscreen:true

            })
                .then(function() {

                })
        };

        self.showToastMessage = function (message, theme) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position('bottom')
                    .theme(theme)
                    .hideDelay(1500)
                    .parent('toastParent')
            );
        }
        //controller for manage popups
        function popupController ($scope) {

            //set values on form
             $scope.editformData = angular.copy(self.selectedItem);
             $scope.editformData.remindDate = new Date(self.selectedItem.remindDate);
             $scope.minDate = self.minDate;

             //edit
            $scope.editReminder = function (editformData) {

                if ($scope.editformData.title != "" &&  $scope.editformData.title != undefined &&
                    $scope.editformData.remindDate != "" && $scope.editformData.remindDate != undefined &&
                    $scope.editformData.description != "" &&  $scope.editformData.description != undefined) {

                    self.showToastMessage('Reminder edited','success-toast' );
                    $mdDialog.cancel();
                }


            };


            $scope.deleteReminder = function () {

                self.showToastMessage('Reminder deleted','success-toast');
                $mdDialog.cancel();

            }

        }
    })