angular.module('clms.attendance-mgt', ['ngMessages']).config(['$mdIconProvider', function ($mdIconProvider) {

    $mdIconProvider.icon('md-close', 'img/icons/ic_close_24px.svg', 24);

}])

    .controller('attendanceCtrl', function (AttendanceFactory, $mdToast, $scope) {

        const self = this;

        self.sAttendance = [];
        self.dateAttendance = new Date();
        self.selectedClass = '';
        self.isShowList = false;
        self.attendanceRecodes = [];
        self.maxDate = new Date();

        //Save Attendance details of a student
        self.saveAttendance = () => {

            self.showToast('success-toast', 'Attendance saved');
            self.Clear();
        }

        //Get all attendance details by passing student class and date
        self.getAttendanceDetailsByFilter = () => {

                self.attendanceRecodes = [];
                console.log(self.selectedClass);

            if(self.selectedClass == "A"){

                self.attendanceRecodes = [
                    {'sName':'Navin Dilshan','sID':'011','aStatus':'Present'},
                    {'sName':'Janaka Herath','sID':'012','aStatus':'Absent'},
                    {'sName':'Pavan Sumith','sID':'013','aStatus':'Present'},
                    {'sName':'Nelum Kumari','sID':'014','aStatus':'Present'}
                ];

            }else if(self.selectedClass == 'B'){

                self.attendanceRecodes = [
                    {'sName':'Darshna Ekanaye','sID':'011','aStatus':'Present'},
                    {'sName':'Nadun Thilina','sID':'012','aStatus':'Absent'},
                    {'sName':'Sanjeew Pandula','sID':'013','aStatus':'Present'},
                    {'sName':'Ayesh Sagara','sID':'014','aStatus':'Present'}
                ];

            }else if(self.selectedClass == 'C'){

                self.attendanceRecodes = [
                    {'sName':'Shanu B lara','sID':'011','aStatus':'Present'},
                    {'sName':'Dinush Niranjan','sID':'012','aStatus':'Absent'},
                    {'sName':'Malika Ishan','sID':'013','aStatus':'Present'},
                    {'sName':'Kasun Kawinda','sID':'014','aStatus':'Present'}
                ];

            }

        }

        //Get all student list by passing student class
        self.getStudentListByClass = () => {

            if(self.selectedClass == "A"){

                self.sAttendance = [{
                    'name':'Navin Dilshan','studentId':'011'
                },
                    {
                        'name':'Janaka Herath','studentId':'012'
                    },
                    {
                        'name':'Pavan Sumith','studentId':'013'
                    },
                    {
                        'name':'Nelum Kumari','studentId':'014'
                    }];

            } else if(self.selectedClass == "B"){

                self.sAttendance = [{
                    'name':'Darshna Ekanaye','studentId':'015'
                },
                    {
                        'name':'Nadun Thilina','studentId':'016'
                    },
                    {
                        'name':'Rasari Abesinhe','studentId':'017'
                    },
                    {
                        'name':'Sanjeew Pandula','studentId':'018'
                    }];

            } else if (self.selectedClass == "C"){

                self.sAttendance = [{
                    'name':'Darshna Ekanaye','studentId':'015'
                },
                    {
                        'name':'Nadun Thilina','studentId':'016'
                    },
                    {
                        'name':'Rasari Abesinhe','studentId':'017'
                    },
                    {
                        'name':'Sanjeew Pandula','studentId':'018'
                    }];
            }
            self.isShowList = true;

        }


        //Clear all the forms and reset values
        this.Clear = () => {

            this.sAttendance = [];
            self.selectedClass = '';

          //  $scope.type.$setPristine();
          //  $scope.type.$setUntouched();
        }

        //Show toast messages
        this.showToast = function (type, message) {

            $mdToast.show(
                $mdToast.simple()

                    .textContent(message)
                    .position('bottom')
                    .theme(type)
                    .hideDelay(1500)
                    .parent('userForm')
            );
        }
    })