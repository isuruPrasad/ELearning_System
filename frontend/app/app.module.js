/**
 * Created by Nandun Bandara on 9/18/17.
 */
'use strict';

angular.module('clms', [
    'clms.routes',
    'clms.core',
    'clms.analytics',
    'clms.attendance-mgt',
    'clms.classroom-mgt',
    'clms.dashboard',
    'clms.grade-mgt',
    'clms.notification-mgt',
    'clms.reminder-mgt',
    'clms.user-mgt',
    'clms.classroom-mgt.controller'
])



    .run(function ($rootScope, $location, Auth, $window) {

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

            if (!Auth.isLoggedIn()) {

              //  $location.path('/login');

             /*   if (toState.url == '/') {

                    $location.path('/');

                } else {



                } */

            } else {
                $location.path('/dashboard/feed')


            }

        })

    })
