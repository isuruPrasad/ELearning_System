/**
 * Created by nandunb on 9/18/17.
 */
'use strict';

angular.module('clms.dashboard',[])

.controller('dashboardCtrl', function($mdSidenav, $mdDialog, $window, $location, Auth, $timeout){

    let self = this;
    self.username = $window.sessionStorage.username;
    self.name = $window.sessionStorage.name;
    self.permission = $window.sessionStorage.permission;
    self.email = $window.sessionStorage.email;
    self.userTitle = null;
    self.isLoading = false;
    self.notices = [];

    self.openUserSideBar = function() {
        $mdSidenav('right').toggle();
    };



    if (self.permission == "admin") {

        self.adminView = true;
        self.teacherView = true;


    } else if (self.permission == "parent") {

        self.studentView = true;

    } else if (self.permission == "teacher") {

        self.teacherView = true;

    }

    // Logout function for dashboard button
    self.logout = function () {

        Auth.logout();
        $location.path('/login');


    }


})

.controller('feedCtrl', function(Dashboard){

    const self = this;
    self.notices = [];



})