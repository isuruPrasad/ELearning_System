'use strict'
angular.module('clms.analytics')

    .controller('analyticsCtrl',function ($mdDialog,$mdToast,Analytics,$window) {

        const self = this;


        let user_role = $window.sessionStorage.getItem('permission');

        if(user_role == 'parent'){
            self.isParent = true;
            self.isTeacher = false;
        }else if(user_role == 'teacher'){
            self.isParent = false;
            self.isTeacher = true;
        }

        // -----student-view marks and student-viewgrades----

        //form details
        self.marksGraphForm = {
            term:"",
            className:"",
            subject:"",
            graphType:""
        };

        //avalability of button/fileds
        self.showSubjects = false;
        self.showSubjects1 = false;
        self.showGraphGenarate = false;
        self.showTeacherTPGraphsShow = false;

        //store field values
        self.classes = ['A','B','C'];
        self.terms = [1,2,3];
        self.GraphTypes = [
            {typeName:"Marks"},
            {typeName:"Grade"}
        ];
        self.subjects = ['Sinahala','Maths','English'];
        self.temSubjects = [];
        self.availableStudents = [];

        //to store init details
        self.initDetails;
        //to store all marks
        self.marks = [];

        //store avarage marks for track performance
        self.currentUserAvg= 85.52;
        self.currentNormalStudentAvg= 45.32;

        //to store grpahs Data
        self.studentName = [];
        self.studentMarks = [];
        self.studentGrade = [];
        self.graphTopic='';

        //store selected values
        self.selectedClass;
        self.selectedTerm;
        self.selectedSubject;
        self.selectedType;

        self.setAllMarks = function () {
            self.showGraphGenarate = true;
        }

        self.showSubjects = function () {
            self.showSubjects1 = true;
        }


        //get logged user name
        self.logedUser = sessionStorage.getItem('username');

        //set visibility of subjects
        self.setTypeOfGraph = function (type) {
console.log(type);
            if(type == "Marks"){

                self.showSubjects1 = true;
                self.selectedType = type;
                self.graphTopic = 'Marks Details'
            }
            else{

                self.showSubjects1 = false;
                self.selectedType = type;
                self.graphTopic = 'Grade Details'
                self.showGraphGenarate = true;

            }
        };


        //show  marks on graph
        self.showOnGraph = function() {

            if(self.selectedType == 'Marks'){

                $mdDialog
                    .show({

                        controller: studentMarksController,
                        templateUrl: 'app/analytics/templates/graph.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose:true,
                        fullscreen:true

                    })
                    .then(function() {

                    })
            }
            else{

                $mdDialog
                    .show({

                        controller: studentGradeController,
                        templateUrl: 'app/analytics/templates/graph.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose:true,
                        fullscreen:true

                    })
                    .then(function() {

                    })

            }


        };

        //controller for manage student Marks-graphs
        function studentMarksController ($scope) {

            //set topic
            $scope.graphTopic =   self.graphTopic;

            //set delay for grpah loading
            setTimeout(function(){
                var ctx = document.getElementById("graph").getContext("2d");
                var myChart = new Chart(ctx, {

                    type: 'bar',
                    data: {
                        labels: ['Navin','Isuru','Shashika','Prasad','Chanaka','Amal'],
                        datasets: [{
                            label: 'Marks Graphs',
                            data: [20,12,15,45,52,41],

                            backgroundColor: [
                                'rgba(260, 50, 132, 0.6)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(199, 159, 64, 0.2)',
                                'rgba(255, 50, 132, 0.2)',
                                'rgba(54, 162, 200, 0.2)',
                                'rgba(255, 206, 75, 0.4)',
                                'rgba(75, 192, 192, 0.8)',
                            ],

                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(199, 159, 64, 0.4)',
                                'rgba(255, 50, 130, 0.4)',
                                'rgba(54, 162, 180, 0.0)',
                                'rgba(255, 206, 75, 0.2)',
                                'rgba(75, 192, 192, 0.4)',
                            ],

                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }, 250);

        }

        //controller for manage student grade graphs
        function studentGradeController ($scope) {

            //set delay for grpah loading
            setTimeout(function(){

                //set topic
                $scope.graphTopic =   self.graphTopic;

                var ctx = document.getElementById("graph").getContext("2d");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Navin','Isuru','Shashika','Prasad','Chanaka','Amal'],
                        datasets: [{
                            label: 'Grade Graph',
                            data: [20,12,15,45,52,41],

                            backgroundColor: [
                                'rgba(260, 50, 132, 0.6)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(199, 159, 64, 0.2)',
                                'rgba(255, 50, 132, 0.2)',
                                'rgba(54, 162, 200, 0.2)',
                                'rgba(255, 206, 75, 0.4)',
                                'rgba(75, 192, 192, 0.8)',
                            ],

                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(199, 159, 64, 0.4)',
                                'rgba(255, 50, 130, 0.4)',
                                'rgba(54, 162, 180, 0.0)',
                                'rgba(255, 206, 75, 0.2)',
                                'rgba(75, 192, 192, 0.4)',
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }, 250);

        }




        // -----student-track-performance----



        //show  marks on graph
        self.studentViewShowOnTrachPerformanceGraph = function() {

            $mdDialog
                .show({

                    controller: studentPerformanceController,
                    templateUrl: 'app/analytics/templates/graph.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose:true,
                    fullscreen:true

                })
                .then(function() {

                })
        };

        //controller for manage student Marks-graphs
        function studentPerformanceController ($scope) {

            //set topic
            $scope.graphTopic =   "Track performance";

            //set delay for grpah loading
            setTimeout(function(){
                var ctx = document.getElementById('graph').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: self.trackPClassNames,
                        datasets: [{
                            label: 'Your Avarage',
                            data: [35,14,17,45,63,25,33],
                            backgroundColor: "rgba(153,255,51,0.4)"
                        }, {
                            label: 'Normal Student Avarage',
                            data: [20,14,75,65,50,35,35],
                            backgroundColor: "rgba(255,153,0,0.4)"
                        }]
                    }
                });
            }, 250);

        }




        //----teacher view controlling

        self.teacherViewClasses=[{
            'class':'A'
        },{
            'class':'B'
        },
            {'class':'C'}];
        self.teacherViewStudents=[];
        self.teacherViewSelectedStudentDetails=[];
        self.teacherViewTerms=[];
        self.teacherViewShowSubjects = false;
        self.teacherViewStudentsOnTrackPerformance = [{'studentIdAndName':'011-Navin'},{'studentIdAndName':'022-Ayesh'},{'studentIdAndName':'023-Yapa'}];


        //set available terms for a given class
        self.teacherViewSetTerms = function (className) {

            var termAvalability=false;
            self.teacherViewTerms=[1,2,3];
            self.teacherViewMarks=[];
            self.teacherViewSelectedClass = className;


        }

        //set available subjects for a given class
        self.teacherViewSetRelaventSubjects = function (term) {

            //set term
            self.teacherViewSelectedTerm = term;
            self.teacherViewSubjects = ['Sinhala','Maths','English'];


        }

        //set marks of all students
        self.teacherViewSetRelevantMarks = function(subject){

            self.teacherViewSelectedSubject = subject;
            self.teacherViewMarks = [];

            var marksDetails = {

                "class":self.teacherViewSelectedClass,
                "term":self.teacherViewSelectedTerm
            }

            self.teacherViewStudentMarks = [
                {'sId':'105','marks':'45'},
                {'sId':'106','marks':'20'},
                {'sId':'107','marks':'17'},
                {'sId':'108','marks':'60'}
            ]
            //show generate button
            self.teacherViewShowGraphGenarate = true;
        }


        //set visibility of subjects
        self.teacherViewSetTypeOfGraph = function (type) {

            if(type == "Marks"){

                self.teacherViewShowSubjects = true;
                self.teacherViewShowSelectedType = type;
                self.teacherViewShowGraphTopic = 'Marks Details'
            }
            else{

                self.teacherViewShowSubjects = false;
                self.teacherViewShowSelectedType = type;
                self.teacherViewShowGraphTopic = 'Grade Details'
                self.teacherViewShowGraphGenarate = true;

            }
        };

        //set grade of all students
        self.teacherViewSetAllGrades = function () {

            self.teacherViewMarks = [];

            var marksDetails = {
                "class":self.teacherViewSelectedClass,
                "term":self.teacherViewSelectedTerm
            }

            //get all marks and student details
            Analytics.getAllMarks(marksDetails).then(function (res) {

                var temp={};
                self.teacherViewMarks = [];

                for(var i=0;i<res.data.message.length;i++){

                    temp={};

                    temp = {
                        "sId" : res.data.message[i].sID,
                        "marks" : res.data.message[i].sGrades[0]
                    }

                    self.teacherViewMarks.push(temp);
                }
                self.teacherViewReturnGradeDetails();

            })
            //show generate button
            self.teacherViewShowGraphGenarate = true;

        }

        //show  marks on graph
        self.teacherViewShowOnGraph = function() {

            if(self.teacherViewShowSelectedType == 'Marks'){

                $mdDialog
                    .show({

                        controller: teacherMarksController,
                        templateUrl: 'app/analytics/templates/graph.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose:true,
                        fullscreen:true

                    })
                    .then(function() {

                    })
            }
            else{

                $mdDialog
                    .show({

                        controller: teacherGradeController,
                        templateUrl: 'app/analytics/templates/graph.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose:true,
                        fullscreen:true

                    })
                    .then(function() {

                    })

            }


        };



        //controller for manage student Marks-graphs
        function teacherMarksController ($scope) {

            //set topic
            $scope.graphTopic =   self.graphTopic;

            //set delay for grpah loading
            setTimeout(function(){
                var ctx = document.getElementById("graph").getContext("2d");
                var myChart = new Chart(ctx, {

                    type: 'bar',
                    data: {
                        labels: ['Navin','Danika','Yapa','Hasthi','Lash','Dilshan','Ayesh','Sagara'],
                        datasets: [{
                            label: 'Marks Graph',
                            data: [15,10,12,14,15,12,17,45],

                            backgroundColor: [
                                'rgba(260, 50, 132, 0.6)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(199, 159, 64, 0.2)',
                                'rgba(255, 50, 132, 0.2)',
                                'rgba(54, 162, 200, 0.2)'
                            ],

                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(199, 159, 64, 0.4)',
                                'rgba(255, 50, 130, 0.4)'
                            ],

                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }, 250);

        }

        //controller for manage student grade graphs
        function teacherGradeController ($scope) {

            //set delay for grpah loading
            setTimeout(function(){

                //set topic
                $scope.graphTopic =   self.graphTopic;

                var ctx = document.getElementById("graph").getContext("2d");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Navin','Danika','Yapa','Hasthi','Lash','Dilshan','Ayesh','Sagara'],

                        datasets: [{
                            label: 'Grade Graph',
                            data:  [25,35,45,16,64,17,28,70],

                            backgroundColor: [
                                'rgba(260, 50, 132, 0.6)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(199, 159, 64, 0.2)',
                                'rgba(255, 50, 132, 0.2)',
                                'rgba(54, 162, 200, 0.2)'
                            ],

                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(199, 159, 64, 0.4)',
                                'rgba(255, 50, 130, 0.4)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }, 250);

        }


        //----------teacher-view-track-performance

        self.teacherViewUserAverage=[];
        self.teacherViewClassAverage = [];
        self.teacherViewStudents = [
            '011','012','0123'
        ];




        //show  marks on graph
        self.teacherViewShowOnTrackPerformanceGraph = function() {

            $mdDialog
                .show({

                    controller: teacherPerformanceController,
                    templateUrl: 'app/analytics/templates/graph.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose:true,
                    fullscreen:true

                })
                .then(function() {

                })
        };

        //controller for manage student Marks-graphs
        function teacherPerformanceController ($scope) {

            //set topic
            $scope.graphTopic =   "Track performance";

            //set delay for grpah loading
            setTimeout(function(){
                var ctx = document.getElementById('graph').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: self.teacherViewTrackPClassNames,
                        datasets: [{
                            label: 'Your Avarage',
                            data: self.teacherViewTrackPUserAvg,
                            backgroundColor: "rgba(153,255,51,0.4)"
                        }, {
                            label: 'Normal Student Avarage',
                            data: self.teacherViewTrackPAvg,
                            backgroundColor: "rgba(255,153,0,0.4)"
                        }]
                    }
                });
            }, 250);

        }


        //-----------student-view-attendance

        self.currentStudentClass='';
        self.currentStudentName='';
        self.allAttendance=[];
        self.currentAttendance=[];
        self.attendanceGraphsDataSetLabel=['Present','Absent'];
        self.attendanceGraphsDataData=[];


        //show  attendance on graph
        self.ShowOnAttendanceGraph = function() {


            $mdDialog
                .show({

                    controller: attendanceController,
                    templateUrl: 'app/analytics/templates/graph.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose:true,
                    fullscreen:true

                })
                .then(function() {

                })

        };



        //controller for manage student attendence-graphs
        function attendanceController ($scope) {

                $scope.graphTopic = 'Attendance Graph';
            //set delay for grpah loading
            setTimeout(function(){
                var ctx = document.getElementById("graph").getContext("2d");
                var myChart = new Chart(ctx, {

                    type: 'pie',
                    data: {
                        labels: ['present','absent'],
                        datasets: [{
                            label: "Population (millions)",
                            backgroundColor: ["#3e95cd","#c45850"],
                            data: [20,30]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Attendance Graph From From 2017.01.01 To '+new Date().getFullYear()+'.'+new Date().getMonth()+'.'+new Date().getDate()
                        }
                    }
                });
            }, 250);

        }

    })


    
